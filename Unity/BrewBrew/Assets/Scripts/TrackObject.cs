﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrackObject : MonoBehaviour
{

    public Boolean TrackPosition = true;
    public Boolean TrackRotation = false;
    public Boolean TrackZPos = false;

    public GameObject Target;

    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Target != null) {
            if (TrackPosition) {
                if (TrackZPos) {
                    transform.position = Target.transform.position;
                } else {
                    transform.position = new Vector3(Target.transform.position.x, Target.transform.position.y, transform.position.z);
                }
            }
            if (TrackRotation) {
                transform.rotation = Target.transform.rotation;
            }
        }
        
    }
}
