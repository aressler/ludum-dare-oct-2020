﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SlidePanel : MonoBehaviour
{

    public float SlideSpeed = 0.5f;
    public enum SlideDirection {
        left,
        right,
        top,
        bottom,
    }

    public GameObject OpenButton;
    public GameObject Center;

    public SlideDirection Direction = SlideDirection.left;

    public GameObject Panel;

    public Vector3 InitialPosition;
    public Vector3 ToPosition;



    // Start is called before the first frame update
    void Start()
    {
        InitialPosition = Panel.transform.position;
        ToPosition = InitialPosition;
        
    }

    // Update is called once per frame
    void Update()
    {
        
    }


    IEnumerator Animate(Boolean setActive) {

        if (setActive) {
            Panel.SetActive(true);
        }

        float timer = 0f;

        while(Vector3.Distance(Panel.transform.position, ToPosition) > 0.01) {
            timer += Time.deltaTime * SlideSpeed;

            Panel.transform.position = Vector3.Lerp(
                Panel.transform.position, ToPosition, timer);

            yield return null;
        }

        if (!setActive) {
            Panel.SetActive(false);
        }

    }

    public void Open() {
        RectTransform centerTrans = Center.GetComponent<RectTransform>();

        switch(Direction) {
            case SlideDirection.left:
                ToPosition = new Vector3(
                    centerTrans.position.x,
                    InitialPosition.y,
                    InitialPosition.z);
                break;

            case SlideDirection.right:
                ToPosition = new Vector3(
                    centerTrans.position.x,
                    InitialPosition.y,
                    InitialPosition.z);
                break;

            default:
                throw new Exception("Slide direction not handled");
        }

        OpenButton.SetActive(false);
        StartCoroutine(Animate(true));
    }

    public void Close() {
        ToPosition = InitialPosition;
        StartCoroutine(Animate(false));
        OpenButton.SetActive(true);
    }
}
