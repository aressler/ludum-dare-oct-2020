﻿using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrewScrollItem : MonoBehaviour
{

    public GameObject Name, TimeRemaining, Value, Rarity;

    private GameEngine.PlayerKeg Keg;


    public void SetKeg(GameEngine.PlayerKeg keg) {
        Keg = keg;

        Name.GetComponent<Text>().text = "Name: " + keg.BrewName;
        TimeRemaining.GetComponent<Text>().text = (keg.TimeRemaining == 0) ? "Ready for sale" : "Days until ready: " + keg.TimeRemaining;
        Value.GetComponent<Text>().text = "Value: $" + keg.Value;
        Rarity.GetComponent<Text>().text = "Rarity: " + keg.Rarity;
    }
}
