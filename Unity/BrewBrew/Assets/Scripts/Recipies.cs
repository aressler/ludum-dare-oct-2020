﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;



public class Recipies {

    public enum Rarities {
        Normal = 1,
        Rare = 2,
        VeryRare = 3,
        Legendary = 4,
    } 

    public class Recipie {
        public string Name;
        public int CashValue;
        public int TimeRequired;
        public int MaltsRequired;
        public int HopsRequired;
        public Rarities Rarity;
    }

    public static List<Recipie> AllRecipies = new List<Recipie>() {
        { new Recipie() { Name = "Pale Ale",             CashValue = 50,   TimeRequired = 10,  MaltsRequired = 8,   HopsRequired = 5,  Rarity = Rarities.Normal    } },
        { new Recipie() { Name = "IPA",                  CashValue = 75,   TimeRequired = 15,  MaltsRequired = 10,  HopsRequired = 8,  Rarity = Rarities.Normal    } },
        { new Recipie() { Name = "Mexican Lager",        CashValue = 30,   TimeRequired = 7,   MaltsRequired = 5,   HopsRequired = 3,  Rarity = Rarities.Normal    } },
        { new Recipie() { Name = "Stout",                CashValue = 50,   TimeRequired = 8,   MaltsRequired = 8,   HopsRequired = 4,  Rarity = Rarities.Normal    } },
        { new Recipie() { Name = "Hefeweizen",           CashValue = 40,   TimeRequired = 7,   MaltsRequired = 5,   HopsRequired = 3,  Rarity = Rarities.Normal    } },

        { new Recipie() { Name = "Hazy Pale Ale",        CashValue = 60,   TimeRequired = 10,  MaltsRequired = 9,   HopsRequired = 5,  Rarity = Rarities.Rare      } },
        { new Recipie() { Name = "Hazy IPA",             CashValue = 80,   TimeRequired = 13,  MaltsRequired = 10,  HopsRequired = 9,  Rarity = Rarities.Rare      } },
        { new Recipie() { Name = "American Wheat",       CashValue = 60,   TimeRequired = 9,   MaltsRequired = 6,   HopsRequired = 4,  Rarity = Rarities.Rare      } },
        { new Recipie() { Name = "Dark Irish Stout",     CashValue = 80,   TimeRequired = 12,  MaltsRequired = 10,  HopsRequired = 5,  Rarity = Rarities.Rare      } },

        { new Recipie() { Name = "Pumpkin Ale",          CashValue = 110,  TimeRequired = 15,  MaltsRequired = 12,  HopsRequired = 5,  Rarity = Rarities.VeryRare  } },
        { new Recipie() { Name = "Christmas Ale",        CashValue = 150,  TimeRequired = 17,  MaltsRequired = 13,  HopsRequired = 8,  Rarity = Rarities.VeryRare  } },

        { new Recipie() { Name = "Nectar of the Yeast",  CashValue = 250,  TimeRequired = 20,  MaltsRequired = 15,  HopsRequired = 10, Rarity = Rarities.Legendary } },
    };

}
