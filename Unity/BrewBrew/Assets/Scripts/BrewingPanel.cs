﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class BrewingPanel : MonoBehaviour
{
    public GameObject Dropdown, DoneButton, CreateButton, Message, ScrollContent, OpenButton, RecipieTracker;


    public GameObject BrewScrollItemPrefab;

    private Vector3 InitialPos;
    private Dictionary<int, Recipies.Recipie> Lookup;

    void InitPanel() {
        GameEngine engine = FindObjectOfType<GameEngine>();

        RecipieTracker.GetComponent<Text>().text = "You found " + engine.PlayerEquipment.KnownRecipies.Count + " of " + Recipies.AllRecipies.Count + " recipies.";

        // Current Brews.
        foreach (Transform child in ScrollContent.transform) {
            Destroy(child.gameObject);
        }

        float totalHeight = 0f;
        foreach (GameEngine.PlayerKeg keg in engine.PlayerKegs) {
            var go = Instantiate(BrewScrollItemPrefab, ScrollContent.transform);
            go.GetComponent<BrewScrollItem>().SetKeg(keg);
            go.transform.localPosition = new Vector3(go.transform.localPosition.x, go.transform.localPosition.y - totalHeight, go.transform.localPosition.z);
            totalHeight += go.GetComponent<RectTransform>().rect.height;
        }

        RectTransform rt = ScrollContent.GetComponent<RectTransform>();
        rt.sizeDelta = new Vector2(rt.sizeDelta.x, totalHeight);


        // Recipies known.
        Dropdown dd = Dropdown.GetComponent<Dropdown>();
        Lookup = new Dictionary<int, Recipies.Recipie>();

        dd.ClearOptions();

        List<Dropdown.OptionData> recipies = new List<Dropdown.OptionData>();

        recipies.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = engine.PlayerEquipment.KnownRecipies.Count == 0 ? "Go find some recipies first" : "-- Choose A Recipie to brew --"
        });


        int i = 1;
        foreach (Recipies.Recipie recipie in engine.PlayerEquipment.KnownRecipies) {
            recipies.Add(new UnityEngine.UI.Dropdown.OptionData() {
                text = "" + recipie.Name + ", " + recipie.MaltsRequired + " Malts, " + recipie.HopsRequired + " Hops, " + recipie.Rarity 
            });
            Lookup[i] = recipie;
            ++i;
        }

        dd.AddOptions(recipies);

    }

    public void ShowPanel() {
        InitPanel();
        CenterPanel();
    }

    public void HidePanel() {
        transform.position = InitialPos;
    }

    private void CenterPanel() {
        Center center = FindObjectOfType<Center>();
        transform.position = center.transform.position;
    }


    void CreateBrew() {
        Dropdown dd = Dropdown.GetComponent<Dropdown>();
        GameEngine engine = FindObjectOfType<GameEngine>();


        if (dd.value == 0 || !Lookup.ContainsKey(dd.value)) {
            AudioClip hungover = Resources.Load<AudioClip>("Audio/hungover");
            GetComponent<AudioSource>().PlayOneShot(hungover);
            Debug.Log("Pick something");
            return;
        }

        Recipies.Recipie recipie = Lookup[dd.value];

        engine.CreateBrew(recipie);
        InitPanel();

    }


    // Start is called before the first frame update
    void Start() {
        InitialPos = transform.position;
        CreateButton.GetComponent<Button>().onClick.AddListener(CreateBrew);
        DoneButton.GetComponent<Button>().onClick.AddListener(HidePanel);
        OpenButton.GetComponent<Button>().onClick.AddListener(ShowPanel);
    }

}
