﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StorePanel : MonoBehaviour
{
    public GameObject Dropdown, DoneButton, SellButton;


    public enum StoreWares {
        Hops,
        Malts,
        EmptyKeg,
        Burner,
        Cooler,
        Fermenter,
        Kettle,
    }

    public class StoreOption {
        public StoreWares Ware;
        public int Cost;
    }

    private Dictionary<int, StoreOption> Lookup { get; set; }

    public void InitPanel() {
        Dropdown dd = Dropdown.GetComponent<Dropdown>();
        dd.ClearOptions();

        GameEngine engine = FindObjectOfType<GameEngine>();
        List<Dropdown.OptionData> storeOptions = new List<Dropdown.OptionData>();

        storeOptions.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "-- Choose A Ware --"
        });

        Lookup = new Dictionary<int, StoreOption>();

        // Hops.
        Lookup[1] = new StoreOption() {
            Cost = Mathf.RoundToInt(UnityEngine.Random.Range(5, 15)),
            Ware = StoreWares.Hops
        };
        storeOptions.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "1 Hops @ $" + Lookup[1].Cost
        });

        // Malts.
        Lookup[2] = new StoreOption() {
            Cost = Mathf.RoundToInt(UnityEngine.Random.Range(5, 15)),
            Ware = StoreWares.Malts
        };
        storeOptions.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "1 Malts @ $" + Lookup[2].Cost
        });

        // Empty Kegs.
        Lookup[3] = new StoreOption() {
            Cost = Mathf.RoundToInt(UnityEngine.Random.Range(35, 65)),
            Ware = StoreWares.EmptyKeg
        };
        storeOptions.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "1 Empty Keg @ $" + Lookup[3].Cost
        });


        // Burner.
        Lookup[4] = new StoreOption() {
            Cost = Mathf.RoundToInt(UnityEngine.Random.Range(45, 75)),
            Ware = StoreWares.Burner
        };
        storeOptions.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "1 Burner @ $" + Lookup[4].Cost
        });

        // Cooler.
        Lookup[5] = new StoreOption() {
            Cost = Mathf.RoundToInt(UnityEngine.Random.Range(35, 75)),
            Ware = StoreWares.Cooler
        };
        storeOptions.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "1 Cooler @ $" + Lookup[5].Cost
        });

        // Fermenter.
        Lookup[6] = new StoreOption() {
            Cost = Mathf.RoundToInt(UnityEngine.Random.Range(25, 45)),
            Ware = StoreWares.Fermenter
        };
        storeOptions.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "1 Fermenter @ $" + Lookup[6].Cost
        });

        // Kettle.
        Lookup[7] = new StoreOption() {
            Cost = Mathf.RoundToInt(UnityEngine.Random.Range(45, 75)),
            Ware = StoreWares.Kettle
        };
        storeOptions.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "1 Brew Kettle @ $" + Lookup[7].Cost
        });


        dd.AddOptions(storeOptions);
    }


    void BuySelected () {
        Dropdown dd = Dropdown.GetComponent<Dropdown>();
        GameEngine engine = FindObjectOfType<GameEngine>();

        if (dd.value != 0 && Lookup.ContainsKey(dd.value)) {

            StoreOption option = Lookup[dd.value];

            if (option.Cost > engine.PlayerCash) {
                GetComponent<AudioSource>().Play();
                Debug.Log("NOT ENOUGH CASH");
                return;
            }

            switch(option.Ware) {
                case StoreWares.Malts:
                    engine.PlayerMalts += 1;
                    break;
                case StoreWares.Hops:
                    engine.PlayerHops += 1;
                    break;
                case StoreWares.EmptyKeg:
                    engine.PlayerEquipment.Kegs += 1;
                    break;
                case StoreWares.Burner:
                    ++engine.PlayerEquipment.Burners;
                    break;
                case StoreWares.Cooler:
                    ++engine.PlayerEquipment.Coolers;
                    break;
                case StoreWares.Fermenter:
                    ++engine.PlayerEquipment.Fermenters;
                    break;
                case StoreWares.Kettle:
                    ++engine.PlayerEquipment.BrewKettles;
                    break;
                default:
                    throw new Exception("Unhandled ware: " + option.Ware);
            }

            // Costs cash.
            engine.PlayerCash -= option.Cost;

            engine.SetCounters();
        }
    }


    // Start is called before the first frame update
    void Start() {
        SellButton.GetComponent<Button>().onClick.AddListener(BuySelected);
    }

}
