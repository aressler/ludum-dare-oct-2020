﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SpecialEvent : MonoBehaviour
{
    public Boolean Permanent = false;
    public Boolean IsResolving { get; set; } = false;
    public Boolean IsResolved { get; set; } = false;

    public enum SpecialEvents {
        Apartments,
        Store,
        BigChops,



        Fbi,

        Hops,
        Malts,
        Recipe,

        Keg,
        Burner,
        Fermenter,
        Cooler,
        Kettle,

        Cash,


    }

    public SpecialEvents WhichSpecial;


    public void Reset() {
        IsResolved = false;
        IsResolving = false;
    }

    void FinishTempEvent() {
        GameEngine engine = FindObjectOfType<GameEngine>();
        IsResolving = false;
        IsResolved = true;
        engine.SetCounters();
        Destroy(this.gameObject);
    }

    public void GiveRecipe() {
        GameEngine engine = FindObjectOfType<GameEngine>();

        // Don't add very rare or legendary recipies.
        foreach (var r in Recipies.AllRecipies) {
            if (r.Rarity != Recipies.Rarities.VeryRare && r.Rarity != Recipies.Rarities.Legendary) {
                Boolean exists = engine.PlayerEquipment.KnownRecipies.Exists((Recipies.Recipie known) => {
                    return known.Name == r.Name;
                });
                if (!exists) {
                    engine.PlayerEquipment.KnownRecipies.Add(r);
                    break;
                }
            }
        }
    }

    public IEnumerator ResolveEvent() {
        GameEngine engine = FindObjectOfType<GameEngine>();

        IsResolving = true;
        IsResolved = false;

        switch (WhichSpecial) {

            case SpecialEvents.Apartments:
                yield return ResolveApartments();
                break;

            case SpecialEvents.Store:
                yield return ResolveStore();
                break;

            case SpecialEvents.BigChops:
                yield return ResolveBigChops();
                break;

            case SpecialEvents.Keg:
                ++engine.PlayerEquipment.Kegs;
                break;
            case SpecialEvents.Burner:
                ++engine.PlayerEquipment.Burners;
                break;
            case SpecialEvents.Fermenter:
                ++engine.PlayerEquipment.Fermenters;
                break;
            case SpecialEvents.Cooler:
                ++engine.PlayerEquipment.Coolers;
                break;
            case SpecialEvents.Kettle:
                ++engine.PlayerEquipment.BrewKettles;
                break;

            case SpecialEvents.Recipe:
                GiveRecipe();
                break;
            case SpecialEvents.Hops:
                ++engine.PlayerHops;
                break;
            case SpecialEvents.Malts:
                ++engine.PlayerMalts;
                break;
            case SpecialEvents.Cash:
                engine.PlayerCash += Mathf.RoundToInt(UnityEngine.Random.Range(1, 10));
                break;

            default:
                throw new Exception("This is not handled: " + WhichSpecial);
        }

        AudioClip clip = Resources.Load<AudioClip>("Audio/bottlepop");
        GetComponent<AudioSource>().PlayOneShot(clip);

        yield return new WaitForSeconds(0.4f);

        if (!Permanent) {
            FinishTempEvent();
        }
    }

    private IEnumerator ResolveBigChops () {
        Debug.Log("Big Chops' House...");

        BigChopsPanel panel = FindObjectOfType<BigChopsPanel>();
        Vector3 initialPos = panel.gameObject.transform.position;
        panel.gameObject.SetActive(true);
        CenterPanel(panel.gameObject);

        panel.DoneButton.GetComponent<Button>().onClick.RemoveAllListeners();
        panel.InitPanel();
        panel.DoneButton.GetComponent<Button>().onClick.AddListener(() => {
            IsResolving = false;
            IsResolved = true;
            panel.transform.position = initialPos;
            Debug.Log("Done Big Chops House");
        });

        yield return null;
    }

    private void CenterPanel(GameObject go) {
        Center center = FindObjectOfType<Center>();
        go.transform.position = center.transform.position;
    }

    private IEnumerator ResolveStore() {
        Debug.Log("Store...");

        StorePanel panel = FindObjectOfType<StorePanel>();
        Vector3 initialPos = panel.gameObject.transform.position;
        panel.gameObject.SetActive(true);
        CenterPanel(panel.gameObject);

        panel.DoneButton.GetComponent<Button>().onClick.RemoveAllListeners();
        panel.InitPanel();
        panel.DoneButton.GetComponent<Button>().onClick.AddListener(() => {
            IsResolving = false;
            IsResolved = true;
            panel.transform.position = initialPos;
            Debug.Log("Done Store");
        });

        yield return null;
    }

    private IEnumerator ResolveApartments () {
        Debug.Log("Apartments...");

        ApartmentsPanel panel = FindObjectOfType<ApartmentsPanel>();
        Vector3 initialPos = panel.gameObject.transform.position;
        panel.gameObject.SetActive(true);
        CenterPanel(panel.gameObject);

        panel.DoneButton.GetComponent<Button>().onClick.RemoveAllListeners();
        panel.InitPanel();
        panel.DoneButton.GetComponent<Button>().onClick.AddListener(() => {
            IsResolving = false;
            IsResolved = true;
            panel.transform.position = initialPos;
            Debug.Log("Done Apartments");
        });

        yield return null;
    }


}
