﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ApartmentsPanel : MonoBehaviour
{

    public GameObject Dropdown, DoneButton, SellButton;

    private Dictionary<int, GameEngine.PlayerKeg> Lookup { get; set; }

    public void InitPanel() {
        Dropdown dd = Dropdown.GetComponent<Dropdown>();
        dd.ClearOptions();

        GameEngine engine = FindObjectOfType<GameEngine>();
        List<Dropdown.OptionData> playerKegs = new List<Dropdown.OptionData>();

        playerKegs.Add(new UnityEngine.UI.Dropdown.OptionData() {
            text = "-- Choose A Keg --"
        });

        Lookup = new Dictionary<int, GameEngine.PlayerKeg>();

        int i = 1;
        foreach (GameEngine.PlayerKeg keg in engine.PlayerKegs) {
            if (keg.TimeRemaining == 0) {
                playerKegs.Add(new UnityEngine.UI.Dropdown.OptionData() {
                    text = "" + keg.BrewName + " @ $" + keg.Value
                });
                Lookup.Add(i, keg);
                ++i;
            }
        }

        dd.AddOptions(playerKegs);
    }





    // Start is called before the first frame update
    void Start() {
        SellButton.GetComponent<Button>().onClick.AddListener(SellSelectedKeg);
    }

    void SellSelectedKeg() {
        Dropdown dd = Dropdown.GetComponent<Dropdown>();
        GameEngine engine = FindObjectOfType<GameEngine>();

        if (dd.value != 0 && Lookup.ContainsKey(dd.value)) {
            GameEngine.PlayerKeg keg = Lookup[dd.value];
            engine.SellKeg(keg);
            InitPanel();
        }

    }

}
