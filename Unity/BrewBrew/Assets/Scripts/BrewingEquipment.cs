﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BrewingEquipment {

    public int Burners = 0;
    public int Fermenters = 0;
    public int BrewKettles = 0;
    public int Coolers = 0;
    public int Kegs = 0;
    public List<Recipies.Recipie> KnownRecipies = new List<Recipies.Recipie>();

}
