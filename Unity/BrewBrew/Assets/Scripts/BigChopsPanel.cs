﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Runtime.ExceptionServices;
using UnityEngine;
using UnityEngine.UI;

public class BigChopsPanel : MonoBehaviour
{

    public static float Probability = 0.25f;

    public GameObject Response, DoneButton, BuyButton;

    private int TradeKegs = 10;
    private Recipies.Recipie TradeRecipie;

    public void InitPanel() {
        float outcome = UnityEngine.Random.Range(0f, 1f);

        Debug.Log("You rolled a " + outcome + " for Big Chops to show");

        DoneButton.GetComponentInChildren<Text>().text = "Maybe Next Time Bud";

        BuyButton.SetActive(false);
        if (outcome > BigChopsPanel.Probability) {
            TradeRecipie = PickRandomGoodRecipie();

            TradeKegs = TradeRecipie.Rarity == Recipies.Rarities.Legendary ? 100 : 2;

            Response.GetComponent<Text>().text = "Hey man,\n\n\tI'll trade you this " +
                TradeRecipie.Rarity + " recipie for " + TradeKegs + " kegs.\n\n" +
                "Recipie: " + TradeRecipie.Name;

            BuyButton.SetActive(true);
        } else {
            Response.GetComponent<Text>().text = "Looks like nobody is home.\n\nTry later.";
        }
    }


    Recipies.Recipie PickRandomGoodRecipie() {
        int outcome = Mathf.RoundToInt(UnityEngine.Random.Range(0f, 5f));

        Recipies.Recipie chosenRecipie = null;
        Recipies.Rarities chosenRarity;

        if (outcome == 0) {
            chosenRarity = Recipies.Rarities.Legendary;
        } else {
            chosenRarity = Recipies.Rarities.VeryRare;
        }

        List<Recipies.Recipie> selections = new List<Recipies.Recipie>();

        foreach (Recipies.Recipie recipie in Recipies.AllRecipies) {
            if (recipie.Rarity == chosenRarity) {
                selections.Add(recipie);
            }
        }

        int randomPick = UnityEngine.Random.Range(0, selections.Count);

        chosenRecipie = selections[randomPick];

        return chosenRecipie;
    }



    void BuySelected() {
        GameEngine engine = FindObjectOfType<GameEngine>();

        Boolean hasRecipie = engine.PlayerEquipment.KnownRecipies.Exists((Recipies.Recipie recipie) => {
            return recipie.Name == TradeRecipie.Name;
        });

        if (hasRecipie) {
            Response.GetComponent<Text>().text = "Oh dude, you already know that one. Come back later, and I might have a new one to trade.";
            BuyButton.SetActive(false);
            return;
        }

        if (TradeKegs > engine.PlayerKegs.Count) {
            AudioClip hungover = Resources.Load<AudioClip>("Audio/hungover");
            GetComponent<AudioSource>().PlayOneShot(hungover);
        } else {

            // Remove kegs from player stash.
            for (int i = 0; i < TradeKegs; ++i) {
                engine.PlayerKegs.RemoveAt(0);
            }

            // Give player new recipie.
            engine.PlayerEquipment.KnownRecipies.Add(TradeRecipie);

            Response.GetComponent<Text>().text = "Bossa Nova!";
            DoneButton.GetComponentInChildren<Text>().text = "Thanks dude!";
            BuyButton.SetActive(false);

            engine.SetCounters();
        }

    }


    // Start is called before the first frame update
    void Start() {
        BuyButton.GetComponent<Button>().onClick.AddListener(BuySelected);
    }

}
