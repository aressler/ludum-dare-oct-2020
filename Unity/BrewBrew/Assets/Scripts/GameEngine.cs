﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameEngine : MonoBehaviour
{

    public class PlayerKeg {
        public string BrewName;
        public int Value;
        public int TimeRemaining;
        public Recipies.Rarities Rarity;
    }

    // Player stuff.
    public BrewingEquipment PlayerEquipment = new BrewingEquipment();
    public int PlayerCash = 0;
    public int PlayerHops = 0;
    public int PlayerMalts = 0;
    public List<PlayerKeg> PlayerKegs = new List<PlayerKeg>();

    // Environment.
    public int Day = 1;

    // Counters in the Panel.
    public GameObject BurnersCounter, FermentersCounter,
        BrewKettlesCounter, CoolersCounter, EmptyKegsCounter,
        HopsCounter, MaltsCounter, CashCounter, FullKegsCounter,
        DaysCounter;
    
    // Game board things.
    public GameObject Squares;
    public GameObject BrewTruck;
    public GameObject RollButton;
    public GameObject Center;
    public GameObject Toast;
    public int BrewTruckSquare = 0;
    public float MoveSpeed = 1f;

    // Special Event Prefabs.
    public GameObject SpecialEventBrewKettle, SpecialEventKeg, SpecialEventBurner, SpecialEventCooler, SpecialEventFermenter,
        SpecialEventCash, SpecialEventHops, SpecialEventMalts, SpecialEventRecipe;

    private List<GameObject> SpecialEventGameObjects = new List<GameObject>();

    private float LerpThreshold = 0.01f;
    private List<GameObject> BoardSquares = new List<GameObject>();
    private Boolean IsBrewTruckMoving = false;
    private GameObject NextSquare { get; set; }

    private int SquaresToMove { get; set; }
    private Vector3 InitialToastPosition { get; set; } 

    // Start is called before the first frame update
    void Start() {
        SpecialEventGameObjects.Add(SpecialEventBrewKettle);
        SpecialEventGameObjects.Add(SpecialEventKeg);
        SpecialEventGameObjects.Add(SpecialEventBurner);
        SpecialEventGameObjects.Add(SpecialEventCooler);
        SpecialEventGameObjects.Add(SpecialEventFermenter);

        SpecialEventGameObjects.Add(SpecialEventCash);
        SpecialEventGameObjects.Add(SpecialEventHops);
        SpecialEventGameObjects.Add(SpecialEventMalts);
        SpecialEventGameObjects.Add(SpecialEventRecipe);

        foreach (Transform child in Squares.transform) {
            if (child.tag == "Board Square") {
                BoardSquares.Add(child.gameObject);
            }
        }

        if (BoardSquares.Count == 0) {
            throw new Exception("Unable to get board square objects");
        }

        InitialToastPosition = Toast.transform.position;

        // Binding in the Inspector loses the this context.
        RollButton.GetComponent<Button>().onClick.AddListener(Roll);

        SquaresToMove = 0;
        InitBrewTruck();
        CreateSpecialEvents();
        PlayerCash = 100;
        SetCounters();


        // DEBUG STUFF
        if (!true) {
            PlayerKegs.Add(new PlayerKeg() { BrewName = "Bad Beer", TimeRemaining = 100, Value = 10, Rarity = Recipies.Rarities.Legendary });
            PlayerKegs.Add(new PlayerKeg() { BrewName = "Bad Beer", TimeRemaining = 100, Value = 10, Rarity = Recipies.Rarities.Legendary });
            PlayerKegs.Add(new PlayerKeg() { BrewName = "Bad Beer", TimeRemaining = 100, Value = 10, Rarity = Recipies.Rarities.Legendary });
            PlayerKegs.Add(new PlayerKeg() { BrewName = "Bad Beer", TimeRemaining = 100, Value = 10, Rarity = Recipies.Rarities.Legendary });
            PlayerKegs.Add(new PlayerKeg() { BrewName = "Bad Beer", TimeRemaining = 100, Value = 10, Rarity = Recipies.Rarities.Legendary });
            PlayerKegs.Add(new PlayerKeg() { BrewName = "Bad Beer", TimeRemaining = 0, Value = 340, Rarity = Recipies.Rarities.Normal });
        }
    }


    public void CreateBrew(Recipies.Recipie recipie) {
        if (
            recipie.HopsRequired > PlayerHops ||
            recipie.MaltsRequired > PlayerMalts ||

            PlayerEquipment.Kegs < 1 ||
            PlayerEquipment.BrewKettles < 1 ||
            PlayerEquipment.Burners < 1 ||
            PlayerEquipment.Coolers < 1 ||
            PlayerEquipment.Fermenters < 1
        ) {
            AudioClip hungover = Resources.Load<AudioClip>("Audio/hungover");
            GetComponent<AudioSource>().PlayOneShot(hungover);
        } else {

            // It takes equipment to brew a brew.
            --PlayerEquipment.Kegs;
            --PlayerEquipment.BrewKettles;
            --PlayerEquipment.Burners;
            --PlayerEquipment.Coolers;
            --PlayerEquipment.Fermenters;

            PlayerHops -= recipie.HopsRequired;
            PlayerMalts -= recipie.MaltsRequired;

            // Create the brew.
            PlayerKegs.Add(new PlayerKeg() {
                BrewName = recipie.Name,
                Rarity = recipie.Rarity,
                TimeRemaining = recipie.TimeRequired,
                Value = recipie.CashValue,
            });

            SetCounters();
        }

    }

    public void SetCounters() {
        BurnersCounter.GetComponent<Text>().text = PlayerEquipment.Burners.ToString();
        FermentersCounter.GetComponent<Text>().text = PlayerEquipment.Fermenters.ToString();
        BrewKettlesCounter.GetComponent<Text>().text = PlayerEquipment.BrewKettles.ToString();
        CoolersCounter.GetComponent<Text>().text = PlayerEquipment.Coolers.ToString();
        EmptyKegsCounter.GetComponent<Text>().text = PlayerEquipment.Kegs.ToString();

        MaltsCounter.GetComponent<Text>().text = "Malts: " + PlayerMalts;
        HopsCounter.GetComponent<Text>().text = "Hops: " + PlayerHops;
        CashCounter.GetComponent<Text>().text = "Cash: $" + PlayerCash;
        FullKegsCounter.GetComponent<Text>().text = "Kegs: " + PlayerKegs.Count;
        DaysCounter.GetComponent<Text>().text = "Day: " + Day;
    }

    /// <summary>
    /// Place the truck at the starting square.
    /// </summary>
    void InitBrewTruck() {
        NextSquare = BoardSquares[0];
        StartCoroutine(MoveBrewTruck());
    }


    IEnumerator MoveBrewTruck() {
        IsBrewTruckMoving = true;
        float timer = 0f;

        while (Vector3.Distance(BrewTruck.transform.position, NextSquare.transform.position) > LerpThreshold) {
            timer += Time.deltaTime * MoveSpeed;

            BrewTruck.transform.position = Vector3.Lerp(
                BrewTruck.transform.position, NextSquare.transform.position, timer);

            BrewTruck.transform.rotation = Quaternion.Slerp(
                BrewTruck.transform.rotation, 
                NextSquare.transform.rotation, timer);

            BrewTruck.transform.localScale = NextSquare.transform.localScale;

            yield return null;
        }

        IsBrewTruckMoving = false;
    }

    IEnumerator ShowToast(string message, float time = 3) {

        Toast.GetComponentInChildren<Text>().text = message;

        float timer = 0f;

        Vector3 toPosition = new Vector3(
            Center.transform.position.x,
            Center.transform.position.y + Center.transform.position.y / 2,
            Center.transform.position.z);

        // Show.
        while (Vector3.Distance(Toast.transform.position, toPosition) > LerpThreshold) {
            timer += Time.deltaTime * 1;

            Toast.transform.position = Vector3.Lerp(
                Toast.transform.position, toPosition, timer);
            yield return null;
        }

        yield return new WaitForSeconds(time);

        // Hide
        while (Vector3.Distance(Toast.transform.position, InitialToastPosition) > LerpThreshold) {
            timer += Time.deltaTime * 1;

            Toast.transform.position = Vector3.Lerp(
                Toast.transform.position, InitialToastPosition, timer);
            yield return null;
        }
    }


    /// <summary>
    /// Roll a random number to move the truck.
    /// </summary>
    /// <returns></returns>
    public void Roll() {
        int roll =  Mathf.RoundToInt(UnityEngine.Random.Range(1f, 6f));
        SquaresToMove = roll;
        RollButton.SetActive(false);
        Debug.Log("You rolled a " + roll);
        StartCoroutine(ShowToast("You rolled a " + roll));
    }

    public void CreateSpecialEvents() {

        for (int i = 0; i < BoardSquares.Count; ++i) {
            if (i == BrewTruckSquare) {
                continue;
            }
            SpecialEvent special = BoardSquares[i].GetComponentInChildren<SpecialEvent>();
            if (special) {
                continue;
            }

            float chance = UnityEngine.Random.Range(0, 1f);

            if (chance > 0.25f) {
                int whichSpecial = Mathf.RoundToInt(UnityEngine.Random.Range(0, SpecialEventGameObjects.Count));
                Debug.Log("Which special = " + whichSpecial);
                var go = Instantiate(SpecialEventGameObjects[whichSpecial], BoardSquares[i].transform);
                go.GetComponent<SpecialEvent>().Permanent = false;
            }
        }


    }

    public void RemoveNonPermanentSpecialEvents() {
        foreach (SpecialEvent special in Squares.GetComponentsInChildren<SpecialEvent>()) {
            if (!special.Permanent) {
                Destroy(special.gameObject);
            }
        }
    }

    public void ResetSpecialEvents() {
        foreach (SpecialEvent special in Squares.GetComponentsInChildren<SpecialEvent>()) {
            special.Reset();
        }
    }

    private IEnumerator ShowRollButtonAfter(float seconds) {
        yield return new WaitForSeconds(seconds);
        RollButton.SetActive(true);
    }

    public void SellKeg(PlayerKeg kegToSell) {
        foreach (PlayerKeg keg in PlayerKegs) {
            if (keg == kegToSell && keg.TimeRemaining == 0) {
                PlayerCash += keg.Value;
                PlayerKegs.Remove(keg);
                SetCounters();
                break;
            }
        }

    }

    // Update is called once per frame
    void Update() {

        // If it is moving, then just let it go.
        if (IsBrewTruckMoving) {
            return;
        }

        // Now the truck has landed at a square, so resolve the square event.
        SpecialEvent special = BoardSquares[BrewTruckSquare].GetComponentInChildren<SpecialEvent>();
        if (special != null) {
            // Special takes some time to resolve.
            if (!special.IsResolved) {
                if (!special.IsResolving) {
                    StartCoroutine(special.ResolveEvent());
                }

                // Do not allow truck to continue moving until resolution is done.
                return;
            }
        }

        // If there additional squares to move, then go.
        if (SquaresToMove > 0) {
            Debug.Log("squares to move " + SquaresToMove);
            BrewTruckSquare = (BrewTruckSquare + 1) % BoardSquares.Count;
            NextSquare = BoardSquares[BrewTruckSquare];
            --SquaresToMove;

            StartCoroutine(MoveBrewTruck());

            // This marks the end of a day.
            if (SquaresToMove == 0) {
                StartCoroutine(ShowRollButtonAfter(1));
                ResetSpecialEvents();
                ++Day;


                if (Day % 3 == 0) {
                    RemoveNonPermanentSpecialEvents();
                    CreateSpecialEvents();
                }

                foreach (PlayerKeg keg in PlayerKegs) {
                    if (keg.TimeRemaining > 0) {
                        --keg.TimeRemaining;
                    }
                }

                SetCounters();
            }
        }

    }
}
